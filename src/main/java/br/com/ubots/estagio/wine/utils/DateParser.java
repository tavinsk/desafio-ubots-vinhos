package br.com.ubots.estagio.wine.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateParser {

    private DateParser(){

    }
    public static Date parse(int year){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String newDate = year+"-01-01";

            return formatter.parse(newDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
