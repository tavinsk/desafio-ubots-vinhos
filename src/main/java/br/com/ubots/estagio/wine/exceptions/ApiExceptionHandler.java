package br.com.ubots.estagio.wine.exceptions;

import br.com.ubots.estagio.wine.exceptions.exception.BadArgumentsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(BadArgumentsException.class)
    public ResponseEntity<ErrorMessage> badArgumentsException (BadArgumentsException e, HttpServletRequest request){

        ErrorMessage error = this.buildErrorMessage(HttpStatus.BAD_REQUEST.value(),
                "Argumento Inválido",
                e.getMessage(),
                request.getRequestURI());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorMessage> entityNotFoundException (EntityNotFoundException e, HttpServletRequest request){

        ErrorMessage error = this.buildErrorMessage(HttpStatus.NOT_FOUND.value(),
                "Entidade(s) Não Encontrada(s)",
                e.getMessage(),
                request.getRequestURI());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    private ErrorMessage buildErrorMessage(int httpStatus, String type, String message, String path) {
        return ErrorMessage.builder()
                .timestamp(Instant.now())
                .status(httpStatus)
                .type(type)
                .message(message)
                .path(path)
                .build();
    }

}
