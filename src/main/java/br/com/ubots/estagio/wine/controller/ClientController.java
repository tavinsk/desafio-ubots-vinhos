package br.com.ubots.estagio.wine.controller;

import br.com.ubots.estagio.wine.entity.Client;
import br.com.ubots.estagio.wine.entity.Wine;
import br.com.ubots.estagio.wine.service.interfaces.ClientService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController {
    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/")
    public String testDeployBitBucket() {
        return "deploy feito pelo bitbucket";
    }

    @GetMapping("/client/greaterTotalValueInPurchases")
    public ResponseEntity<List<Client>> getClientsByGreaterTotalValueInPurchases() {
        List<Client> clients = this.clientService.getClientsOrderedByPurchasesGreater();
        return ResponseEntity.ok(clients);
    }

    @GetMapping("/client/biggestPurchase/{year}")
    public ResponseEntity<Client> getClientWithPurchaseBiggestInTheLastYear(@PathVariable Integer year) {
        Client client = this.clientService.getClientWithPurchaseBiggestForYear(year);
        return ResponseEntity.ok(client);
    }

    @GetMapping("/client/loyal")
    public ResponseEntity<List<Client>> getLoyalClients() {
        List<Client> clients = this.clientService.getLoyalClients();
        return ResponseEntity.ok(clients);
    }

    @GetMapping("/client/{idClient}/recommendWine")
    public ResponseEntity<Wine> recommendWineToClient(@PathVariable("idClient") long idClient) {
        Wine wine = this.clientService.recommendWineToClient(idClient);
        return ResponseEntity.ok(wine);
    }

}
