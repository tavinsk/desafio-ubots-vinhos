package br.com.ubots.estagio.wine.repository;

import br.com.ubots.estagio.wine.entity.Category;
import br.com.ubots.estagio.wine.entity.Wine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WineRepository extends JpaRepository<Wine, String> {
    @Query(value = "SELECT * FROM wine as w WHERE w.category = :#{#category?.name()}", nativeQuery = true)
    List<Wine> findByCategory(@Param("category") Category category);

    @Query(value = "SELECT wine.category FROM client AS c\n" +
            "\tINNER JOIN purchase AS p\n" +
            "\tON p.client_id = c.id\n" +
            "\tINNER JOIN purchase_wine as pw\n" +
            "\tON pw.purchase_code = p.code\n" +
            "\tINNER JOIN wine as wine\n" +
            "\tON wine.code = pw.wine_code\n" +
            "\tWHERE c.id = ?1 \n" +
            "\tGROUP BY wine.category, c.id\n" +
            "\tORDER BY COUNT(wine.category) DESC " +
            "LIMIT 1\n",nativeQuery = true)
    Category getMostPurchasedCategoryByTheCustomerId(long clientId);
}
