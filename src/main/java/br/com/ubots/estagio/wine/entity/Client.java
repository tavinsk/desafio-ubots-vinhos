package br.com.ubots.estagio.wine.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Client {

    @Id
    @GeneratedValue
    private long id;
    @Column(unique = true, nullable = false)
    private String cpf;
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private List<Purchase> purchases = new ArrayList<>();

    public Client(String cpf, String name) {
        this.cpf = cpf;
        this.name = name;
    }

    public Client(long id, String cpf, String name) {
        this.id = id;
        this.cpf = cpf;
        this.name = name;
    }

    public int getQuantityOfPurchases(){
        return this.purchases.size();
    }
}
