package br.com.ubots.estagio.wine.repository;

import br.com.ubots.estagio.wine.entity.Client;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "SELECT SUM(p.total_value) AS totalValue, c.* FROM public.client AS c\n" +
            "\tINNER JOIN public.purchase AS p\n" +
            "\tON p.client_id = c.id\n" +
            "\tGROUP BY c.id\n" +
            "ORDER BY totalValue DESC\n", nativeQuery = true)
    List<Client> findByPurchasesOrderByTotalValue();

    @Query(value = "SELECT client.id, client.cpf, client.name FROM public.client as client\n" +
            "\tINNER JOIN public.purchase AS compra\n" +
            "\tON compra.client_id = client.id\n" +
            "\tWHERE compra.date BETWEEN :initialDate AND :finalDate\n" +
            "\tGROUP BY client.id, compra.code\n" +
            "\tORDER BY compra.total_value DESC" +
            " LIMIT 1 ", nativeQuery = true)
    public Client findBiggestPurchaseByYear(@Param("initialDate")Date initialDate, @Param("finalDate") Date finalDate);

    @Query(value = "SELECT COUNT(\"p\".*) AS quantidadeCompras, c.* from public.client AS c\n" +
            "\tINNER JOIN public.purchase AS p\n" +
            "\tON p.client_id = c.id\n" +
            "\tGROUP BY c.id\n" +
            "ORDER BY quantidadeCompras DESC", nativeQuery = true)
    public List<Client> getLoyalClients();

}
