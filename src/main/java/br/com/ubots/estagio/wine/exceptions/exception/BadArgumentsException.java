package br.com.ubots.estagio.wine.exceptions.exception;

public class BadArgumentsException extends RuntimeException{
    public BadArgumentsException(String message) {
        super(message);
    }
}
