package br.com.ubots.estagio.wine.entity;

public enum Category {
    TINTO, BRANCO, ROSE
}
