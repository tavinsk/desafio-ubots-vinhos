package br.com.ubots.estagio.wine.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Wine {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String code;
    private String name;
    private String variety;
    private String country;

    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private Category category;

    private String harvest;
    private double price;

    @JsonIgnore
    @ManyToMany(mappedBy = "wines", cascade = CascadeType.ALL)
    private List<Purchase> purchases = new ArrayList<>();

}
