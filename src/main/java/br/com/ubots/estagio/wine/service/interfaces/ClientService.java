package br.com.ubots.estagio.wine.service.interfaces;

import br.com.ubots.estagio.wine.entity.Client;
import br.com.ubots.estagio.wine.entity.Wine;

import java.util.List;

public interface ClientService {
    List<Client>getClientsOrderedByPurchasesGreater();
    Client getClientWithPurchaseBiggestForYear(int year);
    List<Client> getLoyalClients();
    Wine recommendWineToClient(long idClient);
}
