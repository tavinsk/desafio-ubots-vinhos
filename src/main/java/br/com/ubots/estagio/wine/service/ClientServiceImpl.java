package br.com.ubots.estagio.wine.service;

import br.com.ubots.estagio.wine.entity.Category;
import br.com.ubots.estagio.wine.entity.Client;
import br.com.ubots.estagio.wine.entity.Wine;
import br.com.ubots.estagio.wine.exceptions.exception.BadArgumentsException;
import br.com.ubots.estagio.wine.repository.ClientRepository;
import br.com.ubots.estagio.wine.repository.WineRepository;
import br.com.ubots.estagio.wine.service.interfaces.ClientService;

import br.com.ubots.estagio.wine.utils.DateParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;

    private WineRepository wineRepository;

    public ClientServiceImpl(ClientRepository clientRepository, WineRepository wineRepository) {
        this.clientRepository = clientRepository;
        this.wineRepository = wineRepository;
    }

    @Override
    public List<Client> getClientsOrderedByPurchasesGreater() {
        List<Client> clients = this.clientRepository.findByPurchasesOrderByTotalValue();
        if (clients.isEmpty()){
            throw new EntityNotFoundException("Não foi encontrado nenhum cliente");
        }
        return clients;
    }

    @Override
    public Client getClientWithPurchaseBiggestForYear(int year){
        if (this.isNegativeYear(year)){
            throw new BadArgumentsException("O ano informado é negativo");
        }

        Date initDate = DateParser.parse(year);
        Date finalDate = DateParser.parse(year+1);
        Client client = this.clientRepository.findBiggestPurchaseByYear(initDate, finalDate);
        if (client == null){
            throw new EntityNotFoundException("Não foi encontrado cliente pelo ano informado");
        }
        return client;
    }



    private boolean isNegativeYear(int year){
        return year < 0 ;
    }

    @Override
    public List<Client> getLoyalClients() {
        List<Client> clients = this.clientRepository.getLoyalClients();
        if (clients.isEmpty()){
            throw new EntityNotFoundException("Não foi encontrado clientes");
        }
        return clients;
    }

    @Override
    public Wine recommendWineToClient(long idClient) {
        Client client = this.clientRepository.findById(idClient)
                .orElseThrow(() -> new EntityNotFoundException("Não foi encontrado nenhum cliente pelo id informado"));

        List<Wine> wines = this.wineRepository.findAll();

        if (wines.isEmpty()){
            throw new EntityNotFoundException("Não foi encontrado nenhum vinho para recomendar");
        }

        if (customerNeverBought(client)){
            return this.recommendRandomWine(wines);
        }

        Category mostPurchaseCategory = this.wineRepository.getMostPurchasedCategoryByTheCustomerId(idClient);
        List<Wine> winesForReccomendation = this.wineRepository.findByCategory(mostPurchaseCategory);

        return this.recommendRandomWine(winesForReccomendation);
    }

    private boolean customerNeverBought(Client client){
        return client.getPurchases().isEmpty();
    }

    private Wine recommendRandomWine(List<Wine> winesForReccomendation) {
        Random random = new Random();
        return winesForReccomendation.get(random.nextInt(winesForReccomendation.size()));
    }
}