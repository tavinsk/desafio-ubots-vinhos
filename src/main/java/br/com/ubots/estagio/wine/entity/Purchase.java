package br.com.ubots.estagio.wine.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Purchase {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String code;

    @Column
    private double totalValue;

    @Temporal(TemporalType.DATE)
    private Date date;

    @JsonIgnore
    @ManyToOne
    private Client client;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "purchase_wine",
            joinColumns = { @JoinColumn(name = "purchaseCode") },
            inverseJoinColumns = { @JoinColumn(name = "wineCode") })
    private List<Wine> wines = new ArrayList<>();


    public void addWineToPurchase(Wine wine){
        this.totalValue += wine.getPrice();

        wine.getPurchases().add(this);
        this.getWines().add(wine);

    }
}
