package br.com.ubots.estagio.wine.service;

import br.com.ubots.estagio.wine.entity.Category;
import br.com.ubots.estagio.wine.entity.Client;
import br.com.ubots.estagio.wine.entity.Purchase;
import br.com.ubots.estagio.wine.entity.Wine;
import br.com.ubots.estagio.wine.exceptions.exception.BadArgumentsException;
import br.com.ubots.estagio.wine.repository.ClientRepository;
import br.com.ubots.estagio.wine.repository.WineRepository;
import br.com.ubots.estagio.wine.utils.DateParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {

    @Mock
    ClientRepository clientRepository;

    @Mock
    WineRepository wineRepository;

    @InjectMocks
    ClientServiceImpl clientService;

    @Test
    public void testClientsOrderedByPurchasesGreater(){
        List<Client> clientsFromRespository = this.createListOfClients();
        Mockito.when(clientRepository.findByPurchasesOrderByTotalValue())
                .thenReturn(clientsFromRespository);

        List<Client> clients = this.clientService.getClientsOrderedByPurchasesGreater();

        Assertions.assertEquals(3, clients.size());
    }

    private List<Client> createListOfClients(){
        List<Client> clients = new ArrayList<>();
        clients.add(new Client("000.000.000-01","Gustavo"));
        clients.add(new Client("000.000.000-02","Maria"));
        clients.add(new Client("000.000.000-03","João"));
        return clients;
    }

    @Test
    public void testNotFoundClientsOrderedByPurchasesGreater(){
        List<Client> notFoundClients = new ArrayList<>();
        Mockito.when(clientRepository.findByPurchasesOrderByTotalValue())
                .thenReturn(notFoundClients);

        Exception exception = Assertions.assertThrows(EntityNotFoundException.class, () ->{
            this.clientService.getClientsOrderedByPurchasesGreater();
        });
        assertTrue(exception.getMessage().equals("Não foi encontrado nenhum cliente"));

    }

    @Test
    public void testClientWithBiggestPurchaseInLastYear(){
        Client client = this.createGenericClient();
        int year = 2016;
        Mockito.when(clientRepository.findBiggestPurchaseByYear(DateParser.parse(2016), DateParser.parse(2017)))
                .thenReturn(client);

        Client biggestBuyer = this.clientService.getClientWithPurchaseBiggestForYear(year);
        Assertions.assertEquals("000.000.000-01", biggestBuyer.getCpf());
    }

    @Test
    public void testExceptionClientByBiggestPurchaseByNegativeYear(){
        int negativeYear = -2016;

        Assertions.assertThrows(BadArgumentsException.class, () ->{
            this.clientService.getClientWithPurchaseBiggestForYear(negativeYear);
        });
    }

    @Test
    public void testClientNotFoundClientByBiggestPurchaseByYear(){
        int year = 3000;

        Assertions.assertThrows(EntityNotFoundException.class, () ->{
            this.clientService.getClientWithPurchaseBiggestForYear(year);
        });
    }

    @Test
    public void testLoyalClients(){
        List<Client> loyalClientsFromRepository = this.createListOfClients();
        Mockito.when(clientRepository.getLoyalClients())
                .thenReturn(loyalClientsFromRepository);

        List<Client> loyalClients = this.clientService.getLoyalClients();

        Assertions.assertEquals(3, loyalClients.size());
    }

    @Test
    public void testLoyalClientsNotFound(){
        List<Client> emptyListClients = new ArrayList<>();
        Mockito.when(clientRepository.getLoyalClients())
                .thenReturn(emptyListClients);


        Assertions.assertThrows(EntityNotFoundException.class,()->{
            this.clientService.getLoyalClients();
        });

    }

    @Test
    public void testNotFoundClientByIdWinesForRecomendation(){
        long clientId = 1;
        Optional<Client> clientEmpty = Optional.empty();
        Mockito.when(clientRepository.findById(clientId)).thenReturn(clientEmpty);

        Exception exception = Assertions.assertThrows(EntityNotFoundException.class, () ->{
            this.clientService.recommendWineToClient(clientId);
        });
        assertTrue(exception.getMessage().equals("Não foi encontrado nenhum cliente pelo id informado"));
    }

    @Test
    public void testNotFoundWinesForRecomendation(){
        long clientId = 1;
        Optional<Client> client = Optional.of(new Client());
        Mockito.when(clientRepository.findById(clientId)).thenReturn(client);
        Mockito.when(wineRepository.findAll()).thenReturn(new ArrayList<Wine>());

        Exception exception = Assertions.assertThrows(EntityNotFoundException.class, () ->{
            this.clientService.recommendWineToClient(clientId);
        });
        assertTrue(exception.getMessage().equals("Não foi encontrado nenhum vinho para recomendar"));

       }

    @Test
    public void testRecommendedWineIsTinto(){
        Category category = Category.TINTO;
        Client client = this.buildClientPurchaseWineByCategory(category);
        this.mockRepositoriesByRecommendationTests(client, category);

        Wine recommendedWine = this.clientService.recommendWineToClient(client.getId());

        Assertions.assertEquals(Category.TINTO, recommendedWine.getCategory());
    }

    @Test
    public void testRecommendedWineIsBranco(){
        Category category = Category.BRANCO;
        Client client = this.buildClientPurchaseWineByCategory(category);
        this.mockRepositoriesByRecommendationTests(client, category);

        Wine recommendedWine = this.clientService.recommendWineToClient(client.getId());

        Assertions.assertEquals(Category.BRANCO, recommendedWine.getCategory());
    }

    @Test
    public void testRecommendedWineIsRose(){
        Category category = Category.ROSE;
        Client client = this.buildClientPurchaseWineByCategory(category);
        this.mockRepositoriesByRecommendationTests(client, category);

        Wine recommendedWine = this.clientService.recommendWineToClient(client.getId());

        Assertions.assertEquals(Category.ROSE, recommendedWine.getCategory());
    }

    private Client buildClientPurchaseWineByCategory(Category category) {
        Client client = this.createGenericClient();
        List<Purchase> purchases = this.buildPurchases(client, category);
        client.setPurchases(purchases);

        return client;
    }

    private List<Purchase> buildPurchases(Client client, Category category) {
        List<Purchase> purchases = new ArrayList<>();
        Purchase purchase1 = Purchase.builder().client(client).code("1").date(new Date()).totalValue(150).build();
        Purchase purchase2 = Purchase.builder().client(client).code("2").date(new Date()).totalValue(100).build();

        purchase1.setWines(this.buildPurchaseSpecificCategory(category));
        purchase2.setWines(this.buildPurchaseAllCategoryWines());

        purchases.add(purchase1);
        purchases.add(purchase2);
        return purchases;
    }

    private List<Wine> buildPurchaseSpecificCategory(Category category) {
        List<Wine> wines = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            wines.add(Wine.builder().category(category).code(Integer.toString(i)).build());
        }
        return wines;
    }

    private List<Wine> buildPurchaseAllCategoryWines() {
        List<Wine> wines = new ArrayList<>();
        wines.add(Wine.builder().category(Category.ROSE).code("4").build());
        wines.add(Wine.builder().category(Category.BRANCO).code("5").build());
        wines.add(Wine.builder().category(Category.TINTO).code("6").build());

        return wines;
    }

    private void mockRepositoriesByRecommendationTests(Client client, Category category) {
        Mockito.when(clientRepository.findById(client.getId()))
                .thenReturn(Optional.of(client));

        Mockito.when(wineRepository.findAll())
                .thenReturn(this.buildPurchaseAllCategoryWines());

        Mockito.when(wineRepository.getMostPurchasedCategoryByTheCustomerId(client.getId()))
                .thenReturn(category);

        Mockito.when(wineRepository.findByCategory(category))
                .thenReturn(this.buildPurchaseSpecificCategory(category));

    }

    @Test
    public void testRecommendationWineWithClientNeverBuy(){
        Client client = this.createGenericClient();
        Mockito.when(clientRepository.findById(client.getId()))
                .thenReturn(Optional.of(client));
        Mockito.when(wineRepository.findAll())
                .thenReturn(this.buildPurchaseAllCategoryWines());

        Wine recommendedWine = this.clientService.recommendWineToClient(client.getId());

        Assertions.assertNotNull(recommendedWine);
    }

    private Client createGenericClient(){
        return new Client(1,"000.000.000-01","Gustavo");
    }

}
