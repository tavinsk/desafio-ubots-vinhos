package br.com.ubots.estagio.wine.repository;

import br.com.ubots.estagio.wine.entity.Category;
import br.com.ubots.estagio.wine.entity.Client;
import br.com.ubots.estagio.wine.entity.Purchase;
import br.com.ubots.estagio.wine.entity.Wine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class WineRepositoryTest {

    @Autowired
    private WineRepository wineRepository;

    @Autowired
    private ClientRepository clientRepository;

    @BeforeEach
    @Transient
    public void setup(){
        Client client1 = new Client(1,"001","Comprador de vinho Tinto");
        Client client2 = new Client(2,"002","Comprador de vinho Branco");
        Client client3 = new Client(3, "003", "Comprador de vinho Rose");

        clientRepository.save(client1);
        clientRepository.save(client2);
        clientRepository.save(client2);

        List<Wine> wines = this.createWines();

        //compra de vinhos para o cliente1
        Purchase purchase1 = Purchase.builder().client(client1).code("001").wines(new ArrayList<>()).build();
        purchase1.addWineToPurchase(wines.get(0));
        purchase1.addWineToPurchase(wines.get(1));
        purchase1.addWineToPurchase(wines.get(2));

        Purchase purchase2 = Purchase.builder().client(client1).code("002").wines(new ArrayList<>()).build();
        purchase2.addWineToPurchase(wines.get(2));

        client1.getPurchases().add(purchase1);
        client1.getPurchases().add(purchase2);
        //compra de vinhos para o cliente2

        Purchase purchase3 = Purchase.builder().client(client2).code("003").wines(new ArrayList<>()).build();
        purchase3.addWineToPurchase(wines.get(3));
        purchase3.addWineToPurchase(wines.get(4));
        purchase3.addWineToPurchase(wines.get(5));

        Purchase purchase4 = Purchase.builder().client(client2).code("004").wines(new ArrayList<>()).build();
        purchase4.addWineToPurchase(wines.get(4));
        purchase4.addWineToPurchase(wines.get(5));

        client2.getPurchases().add(purchase3);
        client2.getPurchases().add(purchase4);

        //compra de vinhos para o cliente3

        Purchase purchase5 = Purchase.builder().client(client3).code("005").wines(new ArrayList<>()).build();
        purchase5.addWineToPurchase(wines.get(6));
        purchase5.addWineToPurchase(wines.get(7));

        Purchase purchase6 = Purchase.builder().client(client3).code("006").wines(new ArrayList<>()).build();
        purchase6.addWineToPurchase(wines.get(8));
        purchase6.addWineToPurchase(wines.get(6));

        client3.getPurchases().add(purchase5);
        client3.getPurchases().add(purchase6);


        this.clientRepository.save(client1);
        this.clientRepository.save(client2);
        this.clientRepository.save(client3);

    }

    private List<Wine> createWines(){
        List<Wine> wines = new ArrayList<>();
        wines.add(Wine.builder().category(Category.TINTO).country("Brasil")
                .harvest("2010").variety("variedade teste 1").price(100).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.TINTO).country("Argentina")
                .harvest("2012").variety("variedade teste 2").price(120).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.TINTO).country("França")
                .harvest("2015").variety("variedade teste 3").price(150).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.ROSE).country("Argentina")
                .harvest("2017").variety("variedade ROSE").price(110).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.ROSE).country("Brasil")
                .harvest("2016").variety("variedade ROSE").price(150).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.ROSE).country("Espanha")
                .harvest("2013").variety("variedade ROSE").price(114).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.BRANCO).country("Chile")
                .harvest("2020").variety("variedade BRANCO").price(80).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.BRANCO).country("Uruguai")
                .harvest("2016").variety("variedade BRANCO").price(150).purchases(new ArrayList<>()).build());
        wines.add(Wine.builder().category(Category.BRANCO).country("Espanha")
                .harvest("2013").variety("variedade BRANCO").price(114).purchases(new ArrayList<>()).build());
        return wines;
    }

    @Test
    public void testFindByCategoryTinto(){
        List<Wine> winesTinto = this.wineRepository.findByCategory(Category.TINTO);

        assertEquals(3, winesTinto.size());
        assertEquals(winesTinto.get(0).getCategory(), Category.TINTO);
        assertEquals(winesTinto.get(1).getCategory(), Category.TINTO);
        assertEquals(winesTinto.get(2).getCategory(), Category.TINTO);
    }

    @Test
    public void testFindByCategoryBranco(){
        List<Wine> winesBranco = this.wineRepository.findByCategory(Category.BRANCO);

        assertEquals(3, winesBranco.size());
        assertEquals(winesBranco.get(0).getCategory(), Category.BRANCO);
        assertEquals(winesBranco.get(1).getCategory(), Category.BRANCO);
        assertEquals(winesBranco.get(2).getCategory(), Category.BRANCO);
    }

    @Test
    public void testFindByCategoryRose(){
        List<Wine> winesRose = this.wineRepository.findByCategory(Category.ROSE);

        assertEquals(3, winesRose.size());
        assertEquals(winesRose.get(0).getCategory(), Category.ROSE);
        assertEquals(winesRose.get(1).getCategory(), Category.ROSE);
        assertEquals(winesRose.get(2).getCategory(), Category.ROSE);
    }

    @Test
    public void testCategoryTintoMostPurchaseByTheCustomerId(){
        long id = 1;

        Category category = this.wineRepository.getMostPurchasedCategoryByTheCustomerId(id);

        assertEquals(Category.TINTO, category);
    }

    @Test
    public void testCategoryRoseMostPurchaseByTheCustomerId(){
        long id = 2;

        Category category = this.wineRepository.getMostPurchasedCategoryByTheCustomerId(id);

        assertEquals(Category.ROSE, category);
    }

    @Test
    public void testCategoryBrancoMostPurchaseByTheCustomerId(){
        long id = 3;

        Category category = this.wineRepository.getMostPurchasedCategoryByTheCustomerId(id);

        assertEquals(Category.BRANCO, category);
    }

}
