package br.com.ubots.estagio.wine.repository;

import br.com.ubots.estagio.wine.entity.Category;
import br.com.ubots.estagio.wine.entity.Client;
import br.com.ubots.estagio.wine.entity.Purchase;
import br.com.ubots.estagio.wine.entity.Wine;
import br.com.ubots.estagio.wine.utils.DateParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.beans.Transient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ClientRepositoryTest {
    @Autowired
    private ClientRepository clientRepository;

    @BeforeEach
    @Transient
    public void setup(){
        try {
            setupDataBase();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private void setupDataBase() throws ParseException {
        Client client1 = new Client(1,"001","Maria-ClienteMaisFiel");
        Client client2 = new Client(2,"002","José");
        Client client3 = new Client(3, "003", "gastou mais dinheiro no total");

        clientRepository.save(client1);
        clientRepository.save(client2);
        clientRepository.save(client2);

        Wine wine1 = Wine.builder().category(Category.TINTO).price(100).purchases(new ArrayList<>()).build();
        Wine wine2 = Wine.builder().category(Category.TINTO).price(50).purchases(new ArrayList<>()).build();
        Wine wine3 = Wine.builder().category(Category.TINTO).price(60).purchases(new ArrayList<>()).build();
        Wine wine4 = Wine.builder().category(Category.TINTO).price(70).purchases(new ArrayList<>()).build();
        Wine wine5 = Wine.builder().category(Category.TINTO).price(80).purchases(new ArrayList<>()).build();
        Wine wine6 = Wine.builder().category(Category.BRANCO).price(100).purchases(new ArrayList<>()).build();
        Wine wine7 = Wine.builder().category(Category.BRANCO).price(130).purchases(new ArrayList<>()).build();
        Wine wine8 = Wine.builder().category(Category.BRANCO).price(190).purchases(new ArrayList<>()).build();
        Wine wine9 = Wine.builder().category(Category.ROSE).price(500).purchases(new ArrayList<>()).build();
        Wine wine10 = Wine.builder().category(Category.ROSE).price(90).purchases(new ArrayList<>()).build();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        //compra de vinhos para o cliente1
        Purchase purchase1 = Purchase.builder()
                .client(client1).code("001")
                .date(formatter.parse("2016-10-12"))
                .wines(new ArrayList<>()).build();
        purchase1.addWineToPurchase(wine2);

        Purchase purchase2 = Purchase.builder()
                .client(client1).code("002")
                .date(formatter.parse("2018-09-15"))
                .wines(new ArrayList<>()).build();
        purchase2.addWineToPurchase(wine3);
        purchase2.addWineToPurchase(wine7);
        purchase2.addWineToPurchase(wine10);

        Purchase purchase3 = Purchase.builder()
                .client(client1)
                .code("003")
                .date(formatter.parse("2016-03-22"))
                .wines(new ArrayList<>()).build();
        purchase3.addWineToPurchase(wine1);

        Purchase purchase4 = Purchase.builder()
                .client(client1)
                .date(formatter.parse("2015-11-25"))
                .code("004").wines(new ArrayList<>()).build();
        purchase4.addWineToPurchase(wine3);

        client1.getPurchases().add(purchase1);
        client1.getPurchases().add(purchase2);
        client1.getPurchases().add(purchase3);
        client1.getPurchases().add(purchase4);

        // compra de vinhos para o cliente2
        Purchase purchase5 = Purchase.builder()
                .client(client2)
                .code("005")
                .date(formatter.parse("2010-07-02"))
                .wines(new ArrayList<>()).build();
        purchase5.addWineToPurchase(wine5);
        purchase5.addWineToPurchase(wine6);

        Purchase purchase6 = Purchase.builder()
                .client(client2)
                .code("006")
                .date(formatter.parse("2015-04-20"))
                .wines(new ArrayList<>()).build();
        purchase6.addWineToPurchase(wine8);
        client2.getPurchases().add(purchase5);
        client2.getPurchases().add(purchase6);

        // compra de vinhos para o cliente3
        Purchase purchase7 = Purchase.builder()
                .client(client3)
                .code("007")
                .date(formatter.parse("2016-01-03"))
                .wines(new ArrayList<>()).build();
        purchase7.addWineToPurchase(wine4);
        purchase7.addWineToPurchase(wine9);
        client3.getPurchases().add(purchase7);

        this.clientRepository.save(client1);
        this.clientRepository.save(client2);
        this.clientRepository.save(client3);
    }

    @Test
    void testClientsBiggestPurchase() {
        List<Client> clients = clientRepository.findByPurchasesOrderByTotalValue();

        assertEquals(3, clients.size());
        assertEquals(clients.get(0).getName(),"gastou mais dinheiro no total");
        assertEquals(clients.get(1).getName(),"Maria-ClienteMaisFiel");
        assertEquals(clients.get(2).getName(),"José");

    }
    @Test
    void testNotFoundClientsBiggestPurchase() {
        clientRepository.deleteAll();

        List<Client> clients = clientRepository.findByPurchasesOrderByTotalValue();

        assertEquals(0, clients.size());
    }

    @Test
    void testClientBiggestPurchaseByYear2016() {
        Date initDate = DateParser.parse(2016);
        Date finalDate = DateParser.parse(2017);

        Client client = clientRepository.findBiggestPurchaseByYear(initDate, finalDate);

        assertEquals(3, client.getId());
    }

    @Test
    void testClientBiggestPurchaseByYear2015() {
        Date initDate = DateParser.parse(2015);
        Date finalDate = DateParser.parse(2016);

        Client client = clientRepository.findBiggestPurchaseByYear(initDate, finalDate);

        assertEquals(2, client.getId());
    }

    @Test
    void testNotFoundClientBiggestPurchaseByYear() {
        Date initDate = DateParser.parse(2100);
        Date finalDate = DateParser.parse(2101);

        Client client = clientRepository.findBiggestPurchaseByYear(initDate, finalDate);

        assertNull(client);
    }


    @Test
    void testOrderLoyalClients() {
        List<Client> clients = clientRepository.getLoyalClients();

        assertEquals(3, clients.size());
        assertEquals(clients.get(0).getName(), "Maria-ClienteMaisFiel");
        assertTrue(clients.get(0).getQuantityOfPurchases() > clients.get(1).getQuantityOfPurchases());
        assertTrue(clients.get(0).getQuantityOfPurchases() > clients.get(2).getQuantityOfPurchases());
        assertTrue(clients.get(1).getQuantityOfPurchases() > clients.get(2).getQuantityOfPurchases());
    }

    @Test
    void testNotFoundLoyalClients() {
        clientRepository.deleteAll();

        List<Client> clients = clientRepository.getLoyalClients();

        assertEquals(0, clients.size());
    }
}
