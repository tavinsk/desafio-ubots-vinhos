package br.com.ubots.estagio.wine.controller;

import br.com.ubots.estagio.wine.entity.Client;
import br.com.ubots.estagio.wine.entity.Wine;
import br.com.ubots.estagio.wine.exceptions.exception.BadArgumentsException;
import br.com.ubots.estagio.wine.repository.ClientRepository;
import br.com.ubots.estagio.wine.service.ClientServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ClientControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ClientServiceImpl clientService;

    @MockBean
    private ClientRepository clientRepository;

    @Test
    public void testControllerGetClientsOrderedByGreaterTotalValueInPurchases() throws Exception {
        List<Client> clients = new ArrayList<>();
        clients.add(new Client("000.000.000-01","Gustavo"));
        clients.add(new Client("000.000.000-02","Maria"));
        clients.add(new Client("000.000.000-03","João"));

        Mockito.when(clientService.getClientsOrderedByPurchasesGreater())
                .thenReturn(clients);

        mvc.perform(get("/client/greaterTotalValueInPurchases")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("000.000.000-01")))
                .andExpect(content().string(containsString("000.000.000-02")))
                .andExpect(content().string(containsString("000.000.000-03")));
    }

    @Test
    public void testNotFoundClientsOrderedByGreaterTotalValueInPurchases() throws Exception {
        List<Client> clients = new ArrayList<>();

        Mockito.when(clientService.getClientsOrderedByPurchasesGreater())
                .thenThrow(new EntityNotFoundException("Não foram encontrados clientes"));

        mvc.perform(get("/client/greaterTotalValueInPurchases")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testReturnClientWithPurchaseBiggestInTheLastYear() throws Exception {
        Client client = new Client("Gustavo", "000.000.000-01");
        int year = 2016;

        Mockito.when(clientService.getClientWithPurchaseBiggestForYear(year))
                .thenReturn(client);

        mvc.perform(get("/client/biggestPurchase/"+year))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().string(containsString("Gustavo")));
    }

    @Test
    public void testExpectExceptionWhenYearIsNegativeNumber() throws Exception {
        int negativeYear = -1000;

        Mockito.when(clientService.getClientWithPurchaseBiggestForYear(negativeYear))
                .thenThrow(new BadArgumentsException("O ano informado é negativo"));

        mvc.perform(get("/client/biggestPurchase/" + negativeYear))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testExpectExceptionWhenNotFoundClientForYear() throws Exception {
        int notFoundYear = 2100;
        String exceptionMessage = "Não foi encontrado cliente pelo ano informado";
        Mockito.when(clientService.getClientWithPurchaseBiggestForYear(notFoundYear))
                .thenThrow(new EntityNotFoundException(exceptionMessage));

        mvc.perform(get("/client/biggestPurchase/" + notFoundYear))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals(exceptionMessage, result.getResolvedException().getMessage()));
    }

    @Test
    public void testLoyalClients() throws Exception {
        List<Client> loyalClientsFromService = new ArrayList<>();
        loyalClientsFromService.add(new Client("000.000.000-01","Gustavo"));
        loyalClientsFromService.add(new Client("000.000.000-02","Maria"));
        loyalClientsFromService.add(new Client("000.000.000-03","João"));

        Mockito.when(clientService.getLoyalClients())
                .thenReturn(loyalClientsFromService);

        mvc.perform(get("/client/loyal/"))
                .andExpect(status().isOk())
                .andExpect(result -> result.getResponse().getContentAsString().contains("Gustavo"));
    }

    @Test
    public void testNotFoundLoyalClients() throws Exception {
        String exceptionMessage = "Não foi encontrado clientes";
        Mockito.when(clientService.getLoyalClients())
                .thenThrow(new EntityNotFoundException(exceptionMessage));

        mvc.perform(get("/client/loyal/"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals(exceptionMessage, result.getResolvedException().getMessage()));
    }

    @Test
    public void testWineRecommendation() throws Exception {
        Wine wine = new Wine();
        wine.setCode("8c76af8e-cde8-45aa-8aae-14e040080371");
        wine.setName("Casa Valduga Raízes");
        wine.setPrice(110);

        long idClient = 1;

        Mockito.when(clientService.recommendWineToClient(idClient))
                .thenReturn(wine);

        mvc.perform(get("/client/"+idClient+"/recommendWine"))
                .andExpect(status().isOk())
                .andExpect(result -> result.getResponse().getContentAsString().contains("Casa Valduga Raízes"));
    }

    @Test
    public void testNotFindWineForRecommendation() throws Exception {
        long idClient = 1;
        String exceptionMessage = "Não foi encontrado nenhum vinho para recomendar";

        Mockito.when(clientService.recommendWineToClient(idClient))
                .thenThrow(new EntityNotFoundException(exceptionMessage));

        mvc.perform(get("/client/"+ idClient +"/recommendWine"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals(exceptionMessage, result.getResolvedException().getMessage()));
    }

    @Test
    public void testNotFindindClientsByIdToRecommendWines() throws Exception {
        long idClient = 1;
        String exceptionMessage = "Não foi encontrado nenhum cliente pelo id informado";

        Mockito.when(clientService.recommendWineToClient(idClient))
                .thenThrow(new EntityNotFoundException(exceptionMessage));

        mvc.perform(get("/client/"+ idClient +"/recommendWine"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals(exceptionMessage, result.getResolvedException().getMessage()));
    }
    
}
